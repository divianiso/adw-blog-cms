@extends('layouts.app')

@section('content')

    @if(Route::current()->getName() != 'trashed-posts.index')
        <div class="d-flex justify-content-end">
            <a href="{{ route('posts.create') }}" class="btn btn-success float-right mb-2"><i class="fa fa-plus"></i>
                Add Post</a>
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">
            Posts
        </div>
        <div class="card-body">
            @if($posts->count()>0)
                <table class="table">
                    <thead>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td>
                                <img src="{{ asset('storage/'.$post->image) }}" width="120px" height="80px"
                                     alt="Gambar Postingan">
                            </td>
                            <td>
                                {{ $post->title }}
                            </td>
                            <td>
                                <a href="{{ route('categories.edit', $post->category->id) }}">
                                {{ $post->category->name }} <!-- category diambil dari 'Post.php' di bagian 'public function category()' -->
                                </a>
                            </td>
                            <td>
                                <div align="center" style="display:flex">
                                    @if(!$post->trashed())
                                        <a href="{{ route('posts.edit', $post->id) }}"
                                           class="btn btn-info btn-sm my-2 mx-2"><i class="fa fa-edit"></i> Edit</a>
                                    @else
                                        <form action="{{ route('restore-posts', $post->id) }}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <button type="submit" class="btn btn-secondary btn-sm my-2 mx-2"><i
                                                    class="fa fa-recycle"></i> Restore
                                            </button>
                                        </form>
                                    @endif

                                    <form action="{{ route('posts.destroy', $post->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm my-2 mx-2">
                                            <i class="fa fa-trash-o"></i>
                                            {{ $post->trashed() ? "Delete" : "Trash" }}
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center">No posts yet.</h3>
            @endif
        </div>
    </div>
@endsection

